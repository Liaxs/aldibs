import React, { Component } from 'react';
import Actions from './Actions'
import List from './List'
import AddVehicle from './AddVehicle'
export default class Index extends Component {
  render() {
    return (
      <div >
		<AddVehicle/>
      	<List history={this.props.history} location={this.props.location}/>

      </div>
    );
  }
}


