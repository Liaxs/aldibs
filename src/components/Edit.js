import React, { Component } from 'react';
import Actions from './Actions'

export default class Edit extends Component {
	constructor(){
		super();
	}
	componentWillMount(){
		this.state = {
			vehicle:{
		  "model": this.props.location.state.model,
		    "brand": this.props.location.state.brand,
		    "passengers_capacity":this.props.location.state.passengers_capacity,
		    "category":this.props.location.state.category,
		    "price":this.props.location.state.price,
		    "year":this.props.location.state.year,
		    "weight":this.props.location.state.weight
		}
		}
	}
  render() {
    return (
       <table Style="width:100%;  text-align: center;">
    	<tbody>
		  <tr>
		    <th></th>
		   	<th></th>
		    <th>passengers_capacity</th>
		    <th>price</th>
		  </tr>
		  <tr>
		  <td>{this.props.location.state.brand}</td>
		  <td>{this.props.location.state.model}</td>
		    <td><input type="text"  name="capacidad" onChange={e=>this.setState({passengers_capacity: e.target.value})} defaultValue={this.state.vehicle.passengers_capacity}/></td>
		    <td><input type="text" name="precio"  onChange={e=>this.setState({price: e.target.value})} defaultValue={this.state.vehicle.price} /></td>
		    <td><Actions actionName="Editar" history={this.props.history} action="finEditar" data={{
		    	id:this.props.location.state.id,
		    	vehicle:{
			    "passengers_capacity":this.state.passengers_capacity,
			    "price":this.state.price,
		    	}
		    }
		 }
		/>
</td>
		 </tr>
		 </tbody>
		</table>
    );
  }
}


