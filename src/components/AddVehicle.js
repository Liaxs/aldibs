import React, { Component } from 'react';
import Actions from './Actions'

export default class AddVehicle extends Component {
	constructor(){
		super();
    	this.brand = this.brand.bind(this);
    	this.model = this.model.bind(this);
    	this.category = this.category.bind(this);
    	this.passengers_capacity = this.passengers_capacity.bind(this);
    	this.price = this.price.bind(this);
    	this.weight = this.weight.bind(this);
    	this.year = this.year.bind(this);
    	this.state = {
    		model: '',
		  	brand: '',
		  	passengers_capacity: '',
		  	category: '',
		  	price: '',
		  	year: '',
		  	weight: ''
    	}
	}

  brand(e) {
    this.setState({
      brand: e.target.value
    });
  }
  model(e) {
    this.setState({
      model: e.target.value
    });
  }
    category(e) {
    this.setState({
      category: e.target.value
    });
  }
    passengers_capacity(e) {
    this.setState({
      passengers_capacity: e.target.value
    });
  }
    price(e) {
    this.setState({
      price: e.target.value
    });
  }
    weight(e) {
    this.setState({
      weight: e.target.value
    });
  }
    year(e) {
    this.setState({
      year: e.target.value
    });
  }
  render() {
//<td><input type="text" onChange={this.category}/></td>
    return (
    	<div>
    	<table Style="width:100%;  text-align: center;">
    	<tbody>
		  <tr>
		    <th>brand</th>
		   	<th>model</th>
		    <th>category</th>
		    <th>passengers_capacity</th>
		    <th>price</th>
		    <th>weight</th>
		    <th>year</th>
		  </tr>
		  <tr>
		    <td><input type="text" onChange={this.brand}/></td>
		    <td><input type="text" onChange={this.model}/></td>
		    
		    <td> <select onChange={this.category}>
				  <option value="0">Motocicleta</option>
				  <option value="1">Automóvil</option>
				  <option value="2">SRV</option>
				  <option value="3">Camioneta</option>
				  <option value="4">Camión</option>
				  <option value="5">Micro</option>
				</select>
			</td>
		    <td><input type="text" onChange={this.passengers_capacity}/></td>
		    <td><input type="text" onChange={this.price}/></td>
		    <td><input type="text" onChange={this.weight}/></td>
		    <td><input type="text" onChange={this.year}/></td>
		 </tr>
		 </tbody>
		</table>
		<Actions actionName="Crear Nuevo" action="crear" data={{
		  vehicle:{
		  "model": this.state.model,
		    "brand": this.state.brand,
		    "passengers_capacity":this.state.passengers_capacity,
		    "category":parseInt(this.state.category),
		    "price":this.state.price,
		    "year":this.state.year,
		    "weight":this.state.weight
		  }
		 }
		}
/>
   </div>
    );
  }
}


