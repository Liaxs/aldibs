import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
  withRouter
} from 'react-router-dom';
import Index from './components/index';
import Edit from './components/Edit';
import createHistory from "history/createBrowserHistory"
 

const history = createHistory();
const location = history.location;

 //<Route exact path="/Dashboard" component={Dashboard}/>


class Rutas extends Component {

   render(){
      return(
       <Router forceRefresh={false} history={history} location={location}>
         <Switch>

           <Route exact path="/" component={Index}/>
           <Route exact path="/Edit" component={Edit}/>

         </Switch>
       </Router>
     );
   }
 }

 export default Rutas;