import React, { Component } from 'react';
import { PostDatos, DeleteDatos, UpdateDatos } from './handleDB'

export default class Actions extends Component {
	constructor(props){
		super(props);
		this.actions = this.actions.bind(this)
	}
	async actions(){
		if (this.props.action == "crear") {
			await PostDatos(this.props.data);
			window.location.reload()
		}
		if (this.props.action == "borrar") {
			DeleteDatos(this.props.data);
		}
		if (this.props.action == "editar") {
			this.props.history.push(
			  '/Edit',
			  this.props.data
			)}
		if (this.props.action == "finEditar") {
			await UpdateDatos(this.props.data.id,this.props.data );
			this.props.history.goBack();
		}
	}
  render() {
    return (
    <div>
	     <button onClick={this.actions}>{this.props.actionName}</button>
	</div>
    );
  }
}


