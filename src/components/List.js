import React, { Component } from 'react';
import { ObtenerDatos } from './handleDB'
import store from './../store'
import Actions from './Actions'

export default class List extends Component {
	constructor(props){
		super(props);
		this.state = {
			data: []
		}
		ObtenerDatos();
		store.subscribe(()=>{
			this.setState({
				data:store.getState().vehicleReducer.data
			});
		});
	}

  render() {
  	let row = [];
  	let dataText =[];
  	this.state.data.forEach(vehiclesInfo=>{
  		row.push(
  		<tr>
		    <td>{vehiclesInfo.id}</td>
		    <td>{vehiclesInfo.brand}</td>
		    <td>{vehiclesInfo.model}</td>
		    <td>{vehiclesInfo.category}</td>
		    <td>{vehiclesInfo.created_at}</td>
		    <td>{vehiclesInfo.passengers_capacity}</td>
		    <td>{vehiclesInfo.price}</td>
		    <td>{vehiclesInfo.updated_at}</td>
		    <td>{vehiclesInfo.weight}</td>
		    <td>{vehiclesInfo.year}</td>
		    <td><Actions action="editar" data={vehiclesInfo} history={this.props.history} location={this.props.location} actionName="Editar" /></td>
		    <td><Actions action="borrar" data={vehiclesInfo.id} actionName="Eliminar"/></td>
		 </tr>
		  );
  	}
  		);

    return (
    	 <table Style="width:100%;  text-align: center;">
		  <tr>
		    <th>id</th>
		    <th>brand</th>
		    <th>model</th>
		    <th>category</th>
		    <th>created_at</th>
		    <th>passengers_capacity</th>
		    <th>price</th>
		    <th>updated_at</th>
		    <th>weight</th>
		    <th>year</th>
		    <th>Editar</th>
		    <th>Eliminar</th>
		  </tr>
		  {row}
		</table> 
    );
  }
}

