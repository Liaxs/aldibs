import store from './../store'

export default null;

export  function ObtenerDatos() {
	let config = {
              method: 'GET',
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
              }
          };
	 fetch('https://shielded-beyond-99836.herokuapp.com/vehicles',config)
      .then((response) => {
      return response.json()
    })
    .then((data) => {
	    store.dispatch({
			type: "VEHICLE",
			data: data
		});
    });
}
export async function PostDatos(data) {
	console.log(data);
	let config = {
              method: 'POST',
              headers: {
                'Accept': 'application/json, text/plain, */*',
    			'Content-Type': 'application/json'
              },
              body: JSON.stringify(data)
          };
      await fetch('https://shielded-beyond-99836.herokuapp.com/vehicles',config)
      .then((response) => {
      return response.json()
    })
    .then((data) => {
    	console.log(data);

    });
	
}
export function DeleteDatos(data) {
	console.log(data);
	let config = {
              method: 'DELETE',
              headers: {
                'Accept': 'application/json, text/plain, */*',
    			'Content-Type': 'application/json'
              },
              body: JSON.stringify(data)
          };
      fetch('https://shielded-beyond-99836.herokuapp.com/vehicles/'+data,config)
      .then((response) => {
        window.location.reload()
      return response.json()
    })
    .then((data) => {

    });
	
}
export async function UpdateDatos(id,data) {
  let config = {
              method: 'PATCH',
              headers: {
                'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
              },
              body: JSON.stringify(data)
          };
     await fetch('https://shielded-beyond-99836.herokuapp.com/vehicles/'+id,config)
      .then((response) => {
      return response.json()
    })
    .then((data) => {
    });
  
}