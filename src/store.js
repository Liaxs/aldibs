import { combineReducers, createStore } from 'redux';

const vehicleReducer = ( state = [], action ) => {
  if (action.type === "VEHICLE") {
      return Object.assign({}, state, {data: action.data })
  }
  return state;
};

const rootReducer = combineReducers({
    vehicleReducer
});
const store = createStore(rootReducer);
export default store;
